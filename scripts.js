function load() {
	const revealBtns = Array.from(document.getElementsByClassName('reveal-btn'));
	revealBtns.forEach((btn) => btn.addEventListener('click', reveal));
}

function reveal(event) {
	const reveals = Array.from(document.getElementsByClassName('reveal'));
	const next = reveals[0];

	if (next) {
		next.classList.remove('reveal');
	}

	if (event) {
		revealBtn = event.currentTarget;

		if (revealBtn) {
			revealBtn.remove();
		}
	}
}

window.addEventListener('load', load);
