## About
I made this presentation to teach some of the basics of exploiting common web vulnerabilities to people at my work. Feel free to modify it to suit your needs and present it at your local workplace/hackerspace/kindergarten.

The presentation includes interactive elements that you can use to show off exploits live during your presentation, along with some bullet points that serve to introduce the vulnerabilities you'll be exploiting.

## Featured vulnerabilities
* Cross-site scripting
* SQL injection
* Directory traversal

## Installation
You can view the presentation by cloning the repo and opening index.html in a web browser.

## How it works
All of the vulnerable code runs in the browser using Javascript. That way, you don't have to actually run a vulnerable server to show off exploits, which lowers the amount of things that could go wrong during your presentation.

## Licensing
The version of AlaSQL included in this repository is copyrighted by Andrey Gershun and Mathias Rangel, and it's licensed under the MIT License.

The version of League Mono included in the repository is copyrighted by Tyler Finck, and it's licensed under SIL Open Font License.

All other files in this repository are dedicated to the public domain, and, to the extent possible under law, the author has waived all copyright and related or neighboring rights to them.
